# _*_ coding: utf-8 _*_
#!/usr/bin/env python
import os
COV = None
if os.environ.get('FLASK_COVERAGE'):
    import coverage
    COV = coverage.coverage(branch=True, include='app/*')
    COV.start()

if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]

from app import create_app, db
from config import config
from flask_script import Manager, Shell
from flask_migrate import Migrate, MigrateCommand

app = create_app(os.getenv('FLASK_CONFIG') or 'default')
manager = Manager(app)
migrate = Migrate(app, db)


def make_shell_context():
    return dict(app=app, db=db)
manager.add_command("shell", Shell(make_context=make_shell_context))
manager.add_command('db', MigrateCommand)


@manager.command
def deploy():
    """Run deployment tasks."""
    from flask_migrate import upgrade
    from app.models import Work, WorkInfo

    # migrate database to latest revision
    upgrade()

#port = os.getenv('VCAP_APP_PORT', '5000')
port = os.getenv('PORT', '5000')
if __name__ == "__main__":
    if os.getenv('FLASK_CONFIG') != 'local':
        app.run(host='0.0.0.0', port=int(port))
    else:
        manager.run()

"""
if __name__ == '__main__':
    manager.run()
"""
