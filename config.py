# _*_ coding: utf-8 _*_
import os
import json
from flask import Flask
from werkzeug import secure_filename

basedir = os.path.abspath(os.path.dirname(__file__))

creds = None
LOCAL_DB_URI = os.environ.get('LOCAL_DB_URI') or None

if 'VCAP_SERVICES' in os.environ:
    dbinfo = json.loads(os.environ['VCAP_SERVICES'])['compose-for-postgresql'][0]  
    dbcred = dbinfo["credentials"]  
else:
    module_dir = os.path.dirname(__file__) 
    file_path = os.path.join(module_dir, '.', 'vcap.json')
    print("Looking for file ", file_path)
    try:
        with open(file_path) as f:
            creds = json.loads(f.read())
    except FileNotFoundError:
        print("Credential File was not found")
    dbinfo = creds['compose-for-postgresql'][0]
    dbcred = dbinfo["credentials"]  

class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'hard to guess string'
    SSL_DISABLE = False
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_RECORD_QUERIES = True
    FLASKY_POSTS_PER_PAGE = 20
    FLASKY_FOLLOWERS_PER_PAGE = 50
    FLASKY_COMMENTS_PER_PAGE = 5
    FLASKY_SLOW_DB_QUERY_TIME=0.5
    UPLOAD_FOLDER = '/main'
    ALLOWED_EXTENSIONS = set(['csv'])

    @staticmethod
    def init_app(app):
        pass

class LocalConfig(Config):
    DEBUG = True
    if LOCAL_DB_URI is None:
        SQLALCHEMY_DATABASE_URI = dbcred['uri']
    else:
        SQLALCHEMY_DATABASE_URI = LOCAL_DB_URI

class DevelopmentConfig(Config):
    DEBUG = True
    if LOCAL_DB_URI is None:
        SQLALCHEMY_DATABASE_URI = dbcred['uri']
    else:
        SQLALCHEMY_DATABASE_URI = LOCAL_DB_URI



config = {
    'local': LocalConfig,
    'development': DevelopmentConfig,
    'default': DevelopmentConfig
}
