# _*_ coding: utf-8 _*_
from datetime import datetime
import hashlib
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from markdown import markdown
import bleach
from flask import current_app, request, url_for
from . import db

class Work(db.Model):
    __tablename__ = 'works'
    id=db.Column(db.Integer, primary_key = True)
    work_name = db.Column(db.Text)
    main_work_id = db.Column(db.Text, unique = True)
    username = db.Column(db.Text)
    password = db.Column(db.Text)
    workspaces = db.relationship('WorkInfo', backref=db.backref('info', cascade = "all, delete"), lazy='joined')
    spacesinfo = db.relationship('SpaceInfo', backref=db.backref('spaceinfo', cascade = "all, delete"), lazy='joined')
    
    def __repr__(self):
        return '<main workspace id is %r>' %self.main_work_id
        
class WorkInfo(db.Model):
    __tablename__ = 'workinfos'
    id=db.Column(db.Integer, primary_key = True)
    intentName = db.Column(db.Text)
    work_id = db.Column(db.Text, db.ForeignKey('works.main_work_id', ondelete='CASCADE'))
    space = db.relationship('SpaceInfo')
    space_info = db.Column(db.Integer, db.ForeignKey('spaceinfos.id'))
    
    def __repr__(self):
        return '<workspaces %r>' %self.intentName
    
class SpaceInfo(db.Model):
    __tablename__ = 'spaceinfos'
    id=db.Column(db.Integer, primary_key = True)
    workspace_name = db.Column(db.Text)
    workspace_id = db.Column(db.Text)
    work_id = db.Column(db.Text, db.ForeignKey('works.main_work_id', ondelete='CASCADE'))
    
    def __repr__(self):
        return '<spaceinfos %r>' %self.id
