# _*_ coding: utf-8 _*_
from flask_wtf import Form
from wtforms import StringField, TextAreaField, BooleanField, SelectField,\
    SubmitField, RadioField
from wtforms.fields.html5 import DateField
from wtforms.validators import Required, Length, Email, Regexp
from wtforms import ValidationError
from flask_pagedown.fields import PageDownField
from ..models import Work


# class NameForm(Form):
#     name = StringField('What is your name?', validators=[Required()])
#     submit = SubmitField('Submit')
# 
# 
# class EditProfileForm(Form):
#     name = StringField('Real name', validators=[Length(0, 64)])
#     location = StringField('Location', validators=[Length(0, 64)])
#     about_me = TextAreaField('About me')
#     submit = SubmitField('Submit')
# 
# 
# class EditProfileAdminForm(Form):
#     email = StringField('Email', validators=[Required(), Length(1, 64),
#                                              Email()])
#     username = StringField('Username', validators=[
#         Required(), Length(1, 64), Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
#                                           'Usernames must have only letters, '
#                                           'numbers, dots or underscores')])
#     confirmed = BooleanField('Confirmed')
#     role = SelectField('Role', coerce=int)
#     name = StringField('Real name', validators=[Length(0, 64)])
#     location = StringField('Location', validators=[Length(0, 64)])
#     about_me = TextAreaField('About me')
#     submit = SubmitField('Submit')
# 
#     def __init__(self, user, *args, **kwargs):
#         super(EditProfileAdminForm, self).__init__(*args, **kwargs)
#         self.role.choices = [(role.id, role.name)
#                              for role in Role.query.order_by(Role.name).all()]
#         self.user = user
# 
#     def validate_email(self, field):
#         if field.data != self.user.email and \
#                 User.query.filter_by(email=field.data).first():
#             raise ValidationError('Email already registered.')
# 
#     def validate_username(self, field):
#         if field.data != self.user.username and \
#                 User.query.filter_by(username=field.data).first():
#             raise ValidationError('Username already in use.')
# 
# 
# class PostForm(Form):
#     body = PageDownField("번역할 글을 입력하세요", validators=[Required()])
#     lang = SelectField("언어를 선택하세요", choices =[(1,'영어->한국어'),(2,'한국어->영어')], coerce=int, validators=[Required()])
#     model = SelectField("모델을 선택하세요", choices =[(1,'News'),(2,'Conversation'),(3,'Legal')], coerce=int, validators=[Required()])
#     submit = SubmitField('Submit')
# 
# 
# class CommentForm(Form):
#     score1 = SelectField("Trans 1 score", choices =[(1,'1'),\
#                                                       (2,'2'),(3,'3'),(4,'4'),(5,'5')], coerce=int, validators=[Required()])
#     score2 = SelectField("Trans 2 score", choices =[(1,'1'),\
#                                                       (2,'2'),(3,'3'),(4,'4'),(5,'5')], coerce=int, validators=[Required()])
#     submit = SubmitField('Submit')
# 
# class SearchLogForm(Form):
#     start = DateField("From", format = '%Y-%m-%d', validators=[Required()])
#     end = DateField("To", format = '%Y-%m-%d', validators=[Required()])
#     query = StringField("Search")
#     column = SelectField("Column Name",
#                          choices =[('all','all'),('id','id'),('cl','class'),('ky','key'),('sim','similarity')\
#                                    ,('question','question')])
#     submit = SubmitField('Submit')
#     
# class SearchFeedForm(Form):
#     start = DateField("From", format = '%Y-%m-%d', validators=[Required()])
#     end = DateField("To", format = '%Y-%m-%d', validators=[Required()])
#     query = StringField("Search")
#     column = SelectField("Column Name",
#                          choices =[('all','all'),('id','id'),('ctg','category'),('ky','key'),('sim','similarity')\
#                                    ,('question','question'),('comment','comment'),('score','score'),('author','author')])
#     submit = SubmitField('Submit')
