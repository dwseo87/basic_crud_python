# _*_ coding: utf-8 _*_
from flask import render_template, redirect, url_for, abort, flash, request,\
    current_app, make_response, current_app
from flask_sqlalchemy import get_debug_queries
from sqlalchemy import  func, cast, String
from . import main
# from .forms import EditProfileForm, EditProfileAdminForm, PostForm,\
#     CommentForm, SearchLogForm, SearchFeedForm
from .Conv_ext import ExtractIntent, ExtractEntity
from .. import db
from ..models import Work, WorkInfo, SpaceInfo
from flask.globals import session
from datetime import datetime, timedelta
from flask import json,jsonify
from pip._vendor.distlib.locators import Page
import urllib, requests, sys, codecs, csv
import os, json, urllib, requests, datetime
from flask import Flask, session, escape, Response, send_file, send_from_directory
from flask_cors import CORS, cross_origin
from watson_developer_cloud import ConversationV1

uri = 'https://gateway.watsonplatform.net/conversation/api/v1/workspaces'

# username = '3e9e80ed-1ecc-4f93-8e17-eb6f5527c85c'
# password = 'ztgHD8eA467l'

# workspace_main = "43a6e96c-ac4b-4853-aa0d-1dbae6bb6895"
# workspace_main = "206c933a-4a8b-45d5-874e-c684e64db985"

@main.after_app_request
def after_request(response):
    for query in get_debug_queries():
        if query.duration >= current_app.config['FLASKY_SLOW_DB_QUERY_TIME']:
            current_app.logger.warning(
                'Slow query: %s\nParameters: %s\nDuration: %fs\nContext: %s\n'
                % (query.statement, query.parameters, query.duration,
                   query.context))
    return response

@main.route('/')
@main.route('/index', methods=['GET', 'POST'])
def index():
    works = Work.query
    page = request.args.get('page', 1, type=int)
    if page == -1:
        page = (works.count() - 1) // \
            current_app.config['FLASKY_COMMENTS_PER_PAGE'] + 1
    pagination = works.order_by(Work.id.desc()).paginate(
        page, per_page=current_app.config['FLASKY_COMMENTS_PER_PAGE'],
        error_out=False)
    works=pagination.items
    try:
        intents=request.json['intents']
    except:
        intents=False
    return render_template('index.html', works=works, pagination=pagination)

@main.route('/add', methods=['POST'])
def add_workspace():
    work_name=request.json['work_name']
    main_id=request.json['main_id']
    username = request.json['username']
    password = request.json['password']
    work=Work(work_name=work_name,
              main_work_id=main_id,
              username = username,
              password = password)
    
    wsInfo_null = SpaceInfo(
        workspace_name = 'None',
        workspace_id = 'None',
        spaceinfo = work)
    db.session.add(wsInfo_null)
    
    try:
        WATSON_URI = uri+'/'+main_id+'/intents?version=2017-02-03&export=true'
        res = requests.get(WATSON_URI, auth=(username, password))
        res = res.json()
        i=res["intents"]
        
        WATSON_URI_2 = uri+'?version=2017-02-03'
        res_2 = requests.get(WATSON_URI_2, auth=(username, password))
        res_2 = res_2.json()
        res_2 = res_2["workspaces"]
        
        default_ws = SpaceInfo.query.join(SpaceInfo.spaceinfo).filter(Work.main_work_id==main_id).first()
        
        for b in res_2:
            wsName = b["name"]
            wsId = b["workspace_id"]
            wsInfo = SpaceInfo(
                workspace_name = wsName,
                workspace_id = wsId,
                spaceinfo = work)
            db.session.add(wsInfo)
        
        for a in i:
            intent = a["intent"]
            info = WorkInfo(
                intentName = intent,
                space = default_ws,
                info=work) # reference 생성을 위해 work 객체를 입력
            db.session.add(info)
    except:
        return json.dumps({'success':False, 'url': '/index'}), 400, {'ContentType':'application/json'}
    
    db.session.commit()
    return json.dumps({'success':True, 'url': '/index'}), 200, {'ContentType':'application/json'}

@main.route('/delete', methods=['POST'])
def delete_workspace():
    work_id=request.json['work_id']
    print(work_id)
    Work.query.filter_by(id=work_id).delete()
    db.session.commit()
    return json.dumps({'success':True, 'url': '/index'}), 200, {'ContentType':'application/json'}

@main.route('/update', methods=['POST'])
def update_workspace():
    work_id=request.json['work_id']
    work_name=request.json['work_name']
    work_id=int(work_id)
    target=Work.query.filter_by(id=work_id).first()
    target.work_name=work_name
    
    username = target.username
    password = target.password
    main_id = target.main_work_id
    
    try:
        WATSON_URI = uri+'/'+main_id+'/intents?version=2017-02-03&export=true'
        res = requests.get(WATSON_URI, auth=(username, password))
        res = res.json()
        i=res["intents"]
        
        WATSON_URI_2 = uri+'?version=2017-02-03'
        res_2 = requests.get(WATSON_URI_2, auth=(username, password))
        res_2 = res_2.json()
        res_2 = res_2["workspaces"]
        
        default_ws = SpaceInfo.query.join(SpaceInfo.spaceinfo).filter(Work.main_work_id==main_id).first()
        spacelist = SpaceInfo.query.join(SpaceInfo.spaceinfo).filter(Work.main_work_id==main_id).all()
        intentlist = WorkInfo.query.join(WorkInfo.info).filter(Work.main_work_id==main_id).all()
        list_s=[]
        list_i=[]
        list_s2 = []
        list_i2 = []
        
        for t in spacelist :
            list_s.append(t.workspace_id)
        for p in intentlist :
            list_i.append(p.intentName)
        
        for b in res_2:
            wsName = b["name"]
            wsId = b["workspace_id"]
            list_s2.append(wsId)
            if wsId not in list_s :
                wsInfo = SpaceInfo(
                    workspace_name = wsName,
                    workspace_id = wsId,
                    spaceinfo = target)
                db.session.add(wsInfo)
        
        for a in i:
            intent = a["intent"]
            list_i2.append(intent)
            if intent not in list_i:
                info = WorkInfo(
                    intentName = intent,
                    space = default_ws,
                    info=target) # reference 생성을 위해 work 객체를 입력
                db.session.add(info)
                
        for d in list_i:
            if d not in list_i2:
                WorkInfo.query.filter_by(intentName=d).delete()
                
        for c in list_s:
            if c != 'None':
                if c not in list_s2:
                    SpaceInfo.query.filter_by(workspace_id=c).delete()
        
    except:
        return json.dumps({'success':False, 'url': '/index'}), 400, {'ContentType':'application/json'}
    
    db.session.commit()
    return json.dumps({'success':True, 'url': '/index'}), 200, {'ContentType':'application/json'}

@main.route('/search', methods=['POST','GET'])
def search_intents():
    works = Work.query
    page = request.args.get('page', 1, type=int)
    if page == -1:
        page = (works.count() - 1) // \
            current_app.config['FLASKY_COMMENTS_PER_PAGE'] + 1
    pagination = works.order_by(Work.id.desc()).paginate(
        page, per_page=current_app.config['FLASKY_COMMENTS_PER_PAGE'],
        error_out=False)
    works=pagination.items
    
    work_id=request.form['token']
    target=Work.query.filter_by(id=work_id).first()
    tmain=target.main_work_id
    intents = WorkInfo.query.join(WorkInfo.info).filter(Work.main_work_id==tmain)
    intents = intents.join(WorkInfo.space).all()
    
    options = SpaceInfo.query.join(SpaceInfo.spaceinfo).filter(Work.main_work_id==tmain).all()
    
    return render_template('index.html', works=works, pagination=pagination, intents=intents, options=options)

@main.route('/extract', methods=['POST','GET'])
def extract():
    works = Work.query
    page = request.args.get('page', 1, type=int)
    if page == -1:
        page = (works.count() - 1) // \
            current_app.config['FLASKY_COMMENTS_PER_PAGE'] + 1
    pagination = works.order_by(Work.id.desc()).paginate(
        page, per_page=current_app.config['FLASKY_COMMENTS_PER_PAGE'],
        error_out=False)
    works=pagination.items
    
    work_id=request.form['token']
    target=Work.query.filter_by(id=work_id).first()
    tmain=target.main_work_id
    
    options = SpaceInfo.query.join(SpaceInfo.spaceinfo).filter(Work.main_work_id==tmain).all()
    
    return render_template('index.html', works=works, pagination=pagination, ext_options=options)

@main.route('/optUpdate', methods=['POST'])
def update_option():
    intent_id=request.json['intent_id']
    option_id=request.json['option_id']
    target=WorkInfo.query.filter_by(id=intent_id).first()
    target.space_info=option_id
    db.session.commit()
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@main.route('/ext_intent', methods=['POST'])
def extract_intent():
    id=request.form['token']
    target=SpaceInfo.query.filter_by(id=id).first()
    wsid=target.workspace_id
    target2=Work.query.filter_by(main_work_id=target.work_id).first()
    mwsid=target2.username
    mwspw=target2.password
    
    return ExtractIntent(username=mwsid, password=mwspw, target_ws=wsid)
     
@main.route('/ext_entity', methods=['POST'])
def extract_entity():
    id=request.form['token']
    target=SpaceInfo.query.filter_by(id=id).first()
    wsid=target.workspace_id
    target2=Work.query.filter_by(main_work_id=target.work_id).first()
    mwsid=target2.username
    mwspw=target2.password
    
    return ExtractEntity(username=mwsid, password=mwspw, target_ws=wsid)    
    

#####################################################################################################################################
def get_Instance(username, password):
    version = "2017-02-03"
    conversation_model = ConversationV1(username=username, password=password, version=version)
    return conversation_model

def Conversation_Message(model_obj, workspace_id, message, context, workspace_main):
    if isinstance(model_obj, ConversationV1) == False:
        return "Not found Conversation Model", {}
    send_message = {'text': message }
    #print send_message
    response = model_obj.message(workspace_id=workspace_id, message_input=send_message, context=context)
    conv_response = json.loads(json.dumps(response, indent=2))
    
        
#      #for test
#     try:
#         main_intent=conv_response["intents"][0]["intent"]
#     except:
#         main_intent="None"
#     conv_response["context"]["main_intent"]=main_intent
#     print(main_intent)
    cont = conv_response["context"]

    if 'sub_intent' in cont.keys():
        sub_intent = conv_response["context"]["sub_intent"]
        conv_response["context"]["main_intent"]=sub_intent
        ws=True
    
    else:
        try:
            main_intent = conv_response["intents"][0]["intent"]
            cont = conv_response["context"]
            print("check1", cont)
            try:    
                context_intent = context["main_intent"]
                if workspace_id != workspace_main:
                    if main_intent != context_intent:
                        conv_response["context"]["main_intent"] = context_intent
                        print("#1")
                        print(context_intent, main_intent)
                        ws = False
                elif main_intent != context_intent:
                    conv_response["context"]["main_intent"]=main_intent
                    ws = True
                    print("#2")
                    print(context_intent, main_intent)
                #수정부분(인텐트 걸고 들어왔을때 같은 인텐트로 분기되지 않던 현상 fix)
                elif main_intent == context_intent:
                    if workspace_id == workspace_main:
                        conv_response["context"]["main_intent"]=main_intent
                        ws = True
                        print("fix")
            except:
                conv_response["context"]["main_intent"] = conv_response["intents"][0]["intent"]
                ws = True
                print(conv_response["context"]["main_intent"])
                print(conv_response["intents"][0]["intent"])
        except:
            tok = "do nothing"
            print(tok, "#1")
     
    try: 
        if ws:
            cvalue = conv_response["context"]["main_intent"]
            print("#3", cvalue)
            target = WorkInfo.query.join(WorkInfo.info).filter(Work.main_work_id==workspace_main).join(WorkInfo.space)
            target = target.filter(WorkInfo.intentName==cvalue).first()
            print('#4', target)
            print(target.space.workspace_id)
            if target.space.workspace_id == 'None':
                conv_response["context"]["workspace_id"]=""
            else:
                conv_response["context"]["workspace_id"]=target.space.workspace_id
            print(conv_response["context"]["workspace_id"])
    except:
        fake_key='do nothing'
        print(fake_key)
        
    #print conv_response
    if len(conv_response["output"]["text"]) > 0:
        text = conv_response["output"]["text"][0]
    else:
        text = ""
    return text, conv_response["context"]

def makeError(code, error, url):
    message = {    "errorCode": str(code), "error": error + " : " + url }
    resp = jsonify(message)
    resp.status_code = code
    return resp

########## cf push conversation-helpdesk -b https://github.com/cloudfoundry/buildpack-python.git ##############
# conversation_model_v1 = get_Instance()

@main.errorhandler(404)
@cross_origin()
def not_found(error=None):
    message = {    "errorCode": "404", "error": "Not Found : " + request.url }
    resp = jsonify(message)
    resp.status_code = 404
    return resp

@main.errorhandler(500)
@cross_origin()
def not_found(error=None):
    message = {    "errorCode": "500", "error": "Internal Server Error : " + request.url }
    resp = jsonify(message)
    resp.status_code = 500
    return resp

@main.route('/question', methods=['POST'])
@cross_origin()
def jsonRequest():

    # workspace Demo v.1.1
    # workspace route test
    status_ret = "400"
    message_ret = "Bad Reques"
    if request.method == 'POST':
#         try:
        params = json.loads(json.dumps(request.json))
        question = params["content"]
        username = params["userName"]
        context = params["context"]
        workspace_main = params["mw"]
        #setting conv=get_instance
        cred = Work.query.filter_by(main_work_id=workspace_main).first()
        print(cred)
        conversation_model_v1 = get_Instance(cred.username, cred.password)
        print(context)
        if "workspace_id" in context.keys():
            workspace_id = context["workspace_id"]
            if len(workspace_id) == 0:
                workspace_id = workspace_main
        else:
            workspace_id = workspace_main
        text, context = Conversation_Message(conversation_model_v1, workspace_id, question, context, workspace_main) 
        if len(context["conversation_id"]) > 0:
            if "workspace_id" in context.keys():
                next_workspace_id = context["workspace_id"]
                if workspace_id != next_workspace_id and len(next_workspace_id) != 0:
                    text, context = Conversation_Message(conversation_model_v1, \
                            next_workspace_id, question, context, workspace_main) 
                    if len(context["conversation_id"]) > 0:
                        return jsonify(answer=text, context=context)
            return jsonify(answer=text, context=context)
        else:
            return makeError(404, "Conversation Not Found", request.url)
#         except:
#             return makeError(500, "Internal Server Error", request.url)
    return makeError(400, "Bad Request", request.url)

    
