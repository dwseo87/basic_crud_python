'''
Created on 2016. 10. 10.

@author: 09287
'''
# -*- coding: utf-8 -*-
import csv, sys, codecs
import json, requests
from flask import Flask, make_response, request, stream_with_context, send_file, redirect, url_for
from werkzeug.datastructures import Headers
from werkzeug.wrappers import Response
import io, os

uri = 'https://gateway.watsonplatform.net/conversation/api/v1/workspaces'


def ExtractIntent(username, password, target_ws):
    abp = os.path.abspath('app/static')
    filepath=os.path.join(abp, 'intents.csv')
    print(filepath)  
    
    f = codecs.open(filepath, 'w', encoding='utf-8')
    r = codecs.open(filepath, 'r', encoding='utf-8')
    header = ['<example>', '<intent>']
           
    writer = csv.DictWriter(f, fieldnames=header)
    writer.writeheader()
    data=[]
           
           
    WATSON_URI = uri+'/'+target_ws+'/intents?version=2017-02-03&export=true'
    res = requests.get(WATSON_URI, auth=(username, password))
    res = res.json()
    i=res["intents"]
    for a in i:
        intent = a["intent"]
            
        WATSON_URI_2 = uri+'/'+target_ws+'/intents/'+intent+'?version=2017-02-03&export=true'
        res_2 = requests.get(WATSON_URI_2, auth=(username, password))
        res_2 = res_2.json()
        j=res_2["examples"]
        for b in j:
            text=b['text']
            data.append({'<example>':text,'<intent>':intent})
    for b in data:
        writer.writerow(b)
          
    return redirect(url_for('static', filename='intents.csv'))
 
def ExtractEntity(username, password, target_ws):
    abp = os.path.abspath('app/static')
    filepath=os.path.join(abp, 'entity.csv')
    f = codecs.open(filepath, 'w', encoding='utf-8')
    r = codecs.open(filepath, 'r', encoding='utf-8')
       
    writer = csv.writer(f, delimiter=',')
    data=[]
      
    WATSON_URI_3 = uri+'/'+target_ws+'?version=2017-02-03&export=true'
    res = requests.get(WATSON_URI_3, auth=(username, password))
    res = res.json()
      
    p=res["entities"]
      
    for a in p:
        entity = a["entity"]
        val = a["values"]
        for v in val:
            value=v["value"]
            data.append(entity)
            data.append(value)
            s = v["synonyms"]
            for b in s:
                b.encode('utf-8')
                data.append(b)
            writer.writerow(data)
            del data[:]
    
    return redirect(url_for('static', filename='entity.csv'))

